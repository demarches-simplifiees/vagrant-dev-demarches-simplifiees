$script_ubuntu = <<-SCRIPT
if [ ! -f /opt/puppetlabs/bin/puppet ]; then
  sudo wget --quiet https://apt.puppetlabs.com/puppet7-release-jammy.deb # Ubuntu 22.04
  sudo dpkg -i puppet7-release-jammy.deb                                 # Ubuntu 22.04
  sudo apt-get update
  sudo apt-get install puppet-agent
fi
SCRIPT

Vagrant.configure("2") do |config|
    # Default values
    default_host_postgresql_port = 5432
    default_host_postgresql_ip   = "127.0.0.1"
    default_host_puma_port       = 3000
    default_host_puma_ip         = "127.0.0.1"

    # Environment variable customizations
    host_postgresql_port = ENV['VAGRANT_HOST_POSTGRESQL_PORT'] ? ENV['VAGRANT_HOST_POSTGRESQL_PORT'] : default_host_postgresql_port
    host_postgresql_ip   = ENV['VAGRANT_HOST_POSTGRESQL_IP']   ? ENV['VAGRANT_HOST_POSTGRESQL_IP' ]  : default_host_postgresql_ip
    host_puma_port       = ENV['VAGRANT_HOST_PUMA_PORT']       ? ENV['VAGRANT_HOST_PUMA_PORT']       : default_host_puma_port
    host_puma_ip         = ENV['VAGRANT_HOST_PUMA_IP']         ? ENV['VAGRANT_HOST_PUMA_IP']         : default_host_puma_ip

    config.vm.provider "virtualbox" do |vb|
      vb.name = "DS_DEV_demarches-simplifiees"
      vb.memory = "4096"
      vb.cpus = "4"
    end
    config.vm.box = "ubuntu/jammy64" # Ubuntu 22.04
    config.vm.network "forwarded_port", id: 'PumaServer',  guest: 3000, host: host_puma_port,       auto_correct: true, host_ip: host_puma_ip
    config.vm.network "forwarded_port", id: 'PostgreSQL',  guest: 5432, host: host_postgresql_port, auto_correct: true, host_ip: host_postgresql_ip
    config.vm.hostname = "ds.example.org"

    config.vm.synced_folder "../demarches-simplifiees.fr", "/shared_dev"
    config.vm.synced_folder "puppet/hieradata/", "/tmp/vagrant-puppet/hieradata"
    config.vm.provision "shell", inline: $script_ubuntu
    config.vm.provision "puppet" do |puppet|
      # documentation
      # https://developer.hashicorp.com/vagrant/docs/provisioning/puppet_apply
      puppet.hiera_config_path = "puppet/hiera.yaml"
      puppet.working_directory = "/tmp/vagrant-puppet"
      puppet.module_path = "puppet/modules"
      puppet.manifest_file = "default.pp"
      puppet.manifests_path= "puppet/manifests"
      # puppet.options = "--debug"
    end


  # Message to show after vagrant up
  ###############################################################################################
  ###############################################################################################
  config.vm.post_up_message = <<-MESSAGE
  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️

  ⚠️   No security has been made for production.        ⚠️
  --------------------------------------------------------

  # LOG - To see application log
  vagrant ssh -c 'cd src; tail -f log/development.log'

  # CONFIG - To see application configuration (environment name, schema version, Ruby/Rails version)
  vagrant ssh -c 'cd src; bin/rake about'

  # CONFIG - To see .env file
  vagrant ssh -c 'cd src; cat .env'

  ------------------------------------------------------------------

  # TEST - Before running ruby tests
  vagrant ssh -c 'sudo -u postgres psql -c  "ALTER USER tps_test WITH SUPERUSER;"'

  # TEST - Run all ruby tests (spec files)
  vagrant ssh -c 'cd src; bin/rspec'

  ------------------------------------------------------------------

  # LINT - Run all Linters (RuboCop, HAML-Lint, SCSS-Lint, ESLint, HAML-Lint)
  vagrant ssh -c 'cd src; bin/rake lint'

  # DATABASE - Launch PostgreSQL interactive terminal
  vagrant ssh -c 'psql'

  ------------------------------------------------------------------

  # UPDATE - Update dependencies and database (useful if you have updated the DS repository)
  vagrant ssh -c 'cd src; bin/update'

  # INSTALL - Finalize installation (1)
  vagrant ssh -c 'cd src; bin/setup'

  # Precompile assets (CSS, JS, ...)
  vagrant ssh -c 'cd src; rails assets:precompile'

  # PUMA - Start server to be able to restart it quickly (use Ctrl-C to stop it)
  vagrant ssh -c 'cd src; RAILS_QUEUE_ADAPTER=delayed_job; bin/rails server --binding=0.0.0.0'

  ------------------------------------------------------------------
  Use 'vagrant port' command line to see port mapping.

  Default :    5432 (guest) --> #{host_postgresql_port} (host)  PostgreSQL  port
               3000 (guest) --> #{host_puma_port} (host)  PUMA server port

  ------------------------------------------------------------------
  Super-Admin dashboard .... http://#{host_puma_ip}:#{host_puma_port}/manager/
  To enable features    .... http://#{host_puma_ip}:#{host_puma_port}/manager/features/
  ------------------------------------------------------------------
  Webmail ........ http://#{host_puma_ip}:#{host_puma_port}/letter_opener
  Webapp  ........ http://#{host_puma_ip}:#{host_puma_port}   -----> fisrt run: see (1)
  ------------------------------------------------------------------
  Login .......... test@exemple.fr
  Password ....... this is a very complicated password !

  Above user has following roles:
  "user", "instructor", "administrator" and "super-admin"
  ------------------------------------------------------------------

  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️
  --------------------------------------------------------
  MESSAGE
  ###############################################################################################
  ###############################################################################################

end

