# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


--------------------------------

## v9.0.1, 2023.07.07

### Changed

- fix: update API Particulier staging URL


--------------------------------

## v9.0.0, 2023.06.14

### Breaking change

> requires a DS software version greater than
> or equal to [`2023-05-31-01`](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-31-01)

- fix: setting API_ENTREPRISE_DEFAULT_SIRET (mandatory to user API Entreprise)
- fix: update `API_ENTREPRISE_URL` environment variable (remove API version)  (#37)
  > see <https://github.com/demarches-simplifiees/demarches-simplifiees.fr/commit/ceae9064e1bf1726f2b>


--------------------------------

## v8.0.0, 2023.06.14

### Breaking change

> requires a DS software version greater than
> or equal to [`2023-05-04-01`](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-01)

- fix: update Puppet to the change of quotation marks in `bin/setup`  (#40)
- fix: update Ruby version (3.2.2 instead of 3.1.2)  (#39)
- fix: add `AR_ENCRYPTION_*` environment variable


--------------------------------

## v7.1.0, 2023.06.14

### Known bugs

> - not working with a DS software version greater than or equal to
>   [`2023-05-04-01`](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-01)

### Added

- feat: allow access to DS database from an IDE (#24)
- feat(Vagrantfile): allow to configure PUMA and PostgreSQL ports

### Changed

- refactor: use a Puppet subdirectory (#38)
- fix(Vagrant): use Ubuntu 22.04 instead of Ubuntu 18.04

### Fixed

- docs(README): add "Warning", "About" and "HTTP ports, URLs and credentials"
- docs: add contributing file


--------------------------------

## v7.0.1, 2023.06.09

### Known bugs

> - not working with a DS software version greater than or equal to
>   [`2023-05-04-01`](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-01)

### Fixed

- #36 Update API Entreprise key (staging environnement)
  <https://api.gouv.fr/documentation/api-entreprise>


--------------------------------

## v7.0.0, 2023.02.15

### Known bugs

> - not working with a DS software version greater than or equal to
>   [`2023-05-04-01`](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/releases/tag/2023-05-04-01)

### Breaking change

- Update nodejs verion (16 instead off 12)
- Enable 'pgcrypto' extension on postgresql databases
  > requires a DS software version greater than
  > or equal to [`2022-05-31-02`](https://github.com/betagouv/demarches-simplifiees.fr/releases/tag/2022-05-31-02)


--------------------------------

## v6.0.0, 2022.04.13

### Breaking change

- **#35** - Update ruby verion (3.1.2 instead off 3.0.3)
  > requires a DS software version greater than
  > or equal to [`2022-04-13-01`](https://github.com/betagouv/demarches-simplifiees.fr/releases/tag/2022-04-13-01)


--------------------------------

## v5.2.0, 2022.01.13

### Added

- Configure Gitlab CI (Vagrant validate, Yamllint, SShellCheck, Markdownlint, ...)
- Vagrant message: add documentation to precompile assets (CSS, JS, ...)
- allow to use API Entreprise (staging server):
  customize API_ENTREPRISE_URL and API_ENTREPRISE_KEY in DS .env file


--------------------------------

## v5.1.0, 2021.12.20

### Added

- allow to use quickly API Particulier (staging server):
  customize API_PARTICULIER_URL and ENCRYPTION_SERVICE_SALT in DS .env file


--------------------------------

## v5.0.0, 2021.12.07

### Breaking change

- **#34** - Update ruby verion (3.0.3 instead off 2.7.1)
  > requires a DS software version greater than
  > or equal to [`2021-11-30-02`](https://github.com/betagouv/demarches-simplifiees.fr/releases/tag/2021-11-30-02)

### Added

- disable OTP for super-admin (DS .env file)


--------------------------------

## v4.0.1, 2021.10.20

### Fixed

- #32 - Add missing depandancy (libicu-dev)


--------------------------------

## v4.0.0, 2021.04.25

### Changed

- #30 - Adapt puppet to modification of bin/setup file
- #29 - Enable clock synchronization (required for OTP)
- #28 - Use Europe/Paris timezone

### Fixed

- #27 - Remove unnecessary Tmux dependency


--------------------------------

## v3.0.0, 2020.11.26

### Fixed

- #25 - Vagrant user can use psql easily
- Improve helping message to show after vagrant up

### Breaking change

- Change VM name (`DEV_demarches-simplifiees` instead off `DemarchesSimplifiees`)
- Change database user/role/password and database name
- Rename source directory (`./src/` instead off `./demarches-simplifiees.fr/`)

### Removed

- Remove unnecessary prerequisite Tmux


--------------------------------

## v2.0.1, 2020.11.02

### Fixed

- #19 - Update ruby verion (2.7.1 instead off 2.6.5)


--------------------------------

## v2.0.0, 2020.08.04

### Breaking change

- #11 - Use VM NAT connection instead of bridge

### Added

- Add helping message to show after vagrant up
- Add CODE_OF_CONDUCT file
- Add CHANGELOG file


--------------------------------

## v1.0.0, 2020.08.02

First release


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Known bugs

### Added

### Changed

### Fixed

### Security

--------------------------------

```
