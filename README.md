# Vagrant DEV for "Démarches Simplifiées" (DS)

## Warning ⚠️

> - Created **only** for **test** or **demonstration** purposes.
> - **No security** has been made for production.


## About

This project lets you build from scratch and in few minutes
a running [**Démarches Simplifiées** software](https://github.com/cloudflare/cfssl) (DS)
in a virtual machine on your laptop.

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Code of Conduct](CODE_OF_CONDUCT.md)
- [License](LICENSE)


## HTTP ports, URLs and credentials

| Service            | Port | URL                                   | User              | Password                                |
|--------------------|------|---------------------------------------|-------------------|-----------------------------------------|
| **DS** Webapp      | 3000 | `http://localhost:3000`               | `test@exemple.fr` | `this is a very complicated password !` |
| **DS** SuperAdmin  | 3000 | `http://localhost:3000/manager/`      | `test@exemple.fr` | `this is a very complicated password !` |
| **Webmail**        | 3000 | `http://localhost:3000/letter_opener` |                   |                                         |


## Databases

| PostgreSQL           | Port | Database         | User         | Password                  |
|----------------------|------|------------------|--------------|---------------------------|
| **DS** database      | 5432 | `ds_pg_database` | `ds_pg_user` | `ds_pg_password_ChangeIt` |
| **DS** test database | 5432 | `tps_test`       | `tps_test`   | `tps_test`                |

Access to **DS** database from your IDE:
> `postgresql://ds_pg_user:ds_pg_password_ChangeIt@localhost:5432/ds_pg_database?serverVersion=14&charset=utf8`


---------------------------------

## Prerequisites

On your computer you need to have installed:

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)
- Git
- a Ruby version management tool like RVM or [RBenv](https://github.com/rbenv/rbenv)
- r10k (`gem install r10k`)
- Internet access
- Bash

For Linux users (debian, ubuntu), do **not** install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download `.deb` from the respective websites.


---------------------------------

## Documentation

see:
[Documentation - DevOps/developer/vagrant](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-documentation/-/tree/main/DevOps/developer/vagrant)

- [Step 1: Prerequisites and build Vagrant box](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-documentation/-/tree/main/DevOps/developer/vagrant/step1_Prerequisites-and-Install.md)
- [Step 2: Last manual steps for installation of Démarches simplifiées](https://gitlab.adullact.net/demarches-simplifiees/demarches-simplifiees-documentation/-/tree/main/DevOps/developer/vagrant/step2_configuration_last_manual_steps.md)


### Boot a virtual machine, download and configure all required items to get a running DS

```bash
# To be executed in the directory containing the Vagrantfile

# Creates and starts the VM (Virtual Machine) according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up          # Then you wait few minutes
                    # depends on your network access and power of your computer

    # Fisrt run ? --> Finalize DS installation, precompile assets (CSS, JS, ...)
    vagrant ssh -c 'cd src; bin/setup'                # Finalize DS installation
    vagrant ssh -c 'cd src; rails assets:precompile'  # Precompile assets (CSS, JS, ...)

# PUMA - Start server to be able to restart it quickly (use Ctrl-C to stop it)
vagrant ssh -c 'cd src; RAILS_QUEUE_ADAPTER=delayed_job; bin/rails server --binding=0.0.0.0'

# Stops gracefully the VM
vagrant halt

# Restart the VM
vagrant up

# Stops the VM and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```


### How to start the VM with a custom PUMA port or a custom PostgreSQL port?

```bash
# Creates and starts the VM (Virtual Machine)
# with some customizations (ports, ...)
# - customize PUMA       port  ---> 3000      (default, port allowed above 1000)
# - customize PostgreSQL port  ---> 5432      (default, port allowed above 1000)
# - customize PUMA       IP    ---> 127.0.0.1
# - customize PostgreSQL IP    ---> 127.0.0.1
VAGRANT_HOST_POSTGRESQL_PORT=5439  \
VAGRANT_HOST_PUMA_PORT=3001        \
VAGRANT_HOST_PUMA_IP=0.0.0.0       \
vagrant up
```
