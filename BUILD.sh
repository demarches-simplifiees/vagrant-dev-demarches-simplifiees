#!/bin/bash

# Enable common error handling options
set -o errexit
set -o nounset
set -o pipefail

# Download all required Puppet modules used to configure the virtual machine
cd ./puppet/ || exit
r10k puppetfile install -v
