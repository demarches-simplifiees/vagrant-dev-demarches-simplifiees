# @summary Install and configure Démarches Simplifiées
#
# Install and configure Démarches Simplifiées, the open source tool for french administration to simply create form
#
# @param _system_username
# Name of the system user for DémarchesSimplifiées
# @param db_user_name
# Name of the Postgres user for the database ds
# @param db_user_password
# Password for DS database Password for the Postgres user ds
# @param _ds_home
# Path for the home directory of the system user ds
# @param db_name
# Name of the database for DémarchesSimplifiées
# @param _ds_db_role
# Postgres role of user and databade for DémarchesSimplifiées
# @param db_host
# Hostname of Postgres relay host
# @param db_port
# Port of Postgres relay host
# @param db_test_name
# Name of the test database
# @param db_test_user_name
# Name of the test database's user
# @param db_test_user_password
# Password of the test database's user
# @param _ds_db_test_role
# Role for the test database
# @param source_code_of_ds
# Repository to download Démarches Simplifiées
# @param preRequistes
# List of packages to be installed beforehand

# #############################################################################
# Variables

# System user for Démarches Simplifiées
$_system_username = 'vagrant'
$_ds_home = "/home/${_system_username}"
$_ds_project_directory = "$_ds_home/src"
$db_user_name = 'ds_pg_user'
$db_name = 'ds_pg_database'
$_ds_db_role = 'ds_db_role'
$db_user_password = 'ds_pg_password_ChangeIt' # /!\ Do not forget to change this passwd in production :)
$db_host = 'localhost'
$db_port = 5432
$db_test_name = 'tps_test'
$db_test_user_name = 'tps_test'
$db_test_user_password = 'tps_test'
$_ds_db_test_role = 'ds_db_test_role'
#$source_code_of_ds = 'https://github.com/betagouv/demarches-simplifiees.fr.git'
$local_source_code_of_ds = '/shared_dev/'
$_path_bundle = "$_ds_home/rbenv/shims/"
$db_database = 'ds_pg_database'
$db_pool = ''

# Environment variable ENCRYPTION_SERVICE_SALT (see: .env file)
# is mandatory to use API Particulier
# /!\ Do not forget to change this value in production
$ds_encryption_service_salt = 'aef3153a9829fa4ba10acb02927ac855df6b92795b1ad265d654443c4b14a017'

# Environment variable API_ENTREPRISE_KEY  (see: .env file)
# Staging token (see:  https://api.gouv.fr/documentation/api-entreprise)
$ds_api_entreprise_key = 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI1MmE1YmZjMi1jMzUwLTQ4ZjQtYjY5Ni05ZWE3NmRiM2VmMjkiLCJqdGkiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJzY29wZXMiOlsiY2VydGlmaWNhdF9yZ2VfYWRlbWUiLCJtc2FfY290aXNhdGlvbnMiLCJlbnRyZXByaXNlcyIsImV4dHJhaXRzX3JjcyIsImNlcnRpZmljYXRfb3BxaWJpIiwiYXNzb2NpYXRpb25zIiwiZXRhYmxpc3NlbWVudHMiLCJmbnRwX2NhcnRlX3BybyIsInF1YWxpYmF0IiwiZW50cmVwcmlzZXNfYXJ0aXNhbmFsZXMiLCJjZXJ0aWZpY2F0X2NuZXRwIiwiZW9yaV9kb3VhbmVzIiwicHJvYnRwIiwiYWN0ZXNfaW5waSIsImV4dHJhaXRfY291cnRfaW5waSIsImF0dGVzdGF0aW9uc19zb2NpYWxlcyIsImxpYXNzZV9maXNjYWxlIiwiYXR0ZXN0YXRpb25zX2Zpc2NhbGVzIiwiZXhlcmNpY2VzIiwiY29udmVudGlvbnNfY29sbGVjdGl2ZXMiLCJiaWxhbnNfaW5waSIsImRvY3VtZW50c19hc3NvY2lhdGlvbiIsImNlcnRpZmljYXRfYWdlbmNlX2JpbyIsImJpbGFuc19lbnRyZXByaXNlX2JkZiIsImF0dGVzdGF0aW9uc19hZ2VmaXBoIiwibWVzcmlfaWRlbnRpZmlhbnQiLCJtZXNyaV9pZGVudGl0ZSIsIm1lc3JpX2luc2NyaXB0aW9uX2V0dWRpYW50IiwibWVzcmlfaW5zY3JpcHRpb25fYXV0cmUiLCJtZXNyaV9hZG1pc3Npb24iLCJtZXNyaV9ldGFibGlzc2VtZW50cyIsInBvbGVfZW1wbG9pX2lkZW50aXRlIiwicG9sZV9lbXBsb2lfYWRyZXNzZSIsInBvbGVfZW1wbG9pX2NvbnRhY3QiLCJwb2xlX2VtcGxvaV9pbnNjcmlwdGlvbiIsImNuYWZfcXVvdGllbnRfZmFtaWxpYWwiLCJjbmFmX2FsbG9jYXRhaXJlcyIsImNuYWZfZW5mYW50cyIsImNuYWZfYWRyZXNzZSIsImNub3VzX3N0YXR1dF9ib3Vyc2llciIsInVwdGltZSIsImNub3VzX2VjaGVsb25fYm91cnNlIiwiY25vdXNfZW1haWwiLCJjbm91c19wZXJpb2RlX3ZlcnNlbWVudCIsImNub3VzX3N0YXR1dF9ib3Vyc2UiLCJjbm91c192aWxsZV9ldHVkZXMiLCJjbm91c19pZGVudGl0ZSIsImRnZmlwX2RlY2xhcmFudDFfbm9tIiwiZGdmaXBfZGVjbGFyYW50MV9ub21fbmFpc3NhbmNlIiwiZGdmaXBfZGVjbGFyYW50MV9wcmVub21zIiwiZGdmaXBfZGVjbGFyYW50MV9kYXRlX25haXNzYW5jZSIsImRnZmlwX2RlY2xhcmFudDJfbm9tIiwiZGdmaXBfZGVjbGFyYW50Ml9ub21fbmFpc3NhbmNlIiwiZGdmaXBfZGVjbGFyYW50Ml9wcmVub21zIiwiZGdmaXBfZGVjbGFyYW50Ml9kYXRlX25haXNzYW5jZSIsImRnZmlwX2RhdGVfcmVjb3V2cmVtZW50IiwiZGdmaXBfZGF0ZV9ldGFibGlzc2VtZW50IiwiZGdmaXBfYWRyZXNzZV9maXNjYWxlX3RheGF0aW9uIiwiZGdmaXBfYWRyZXNzZV9maXNjYWxlX2FubmVlIiwiZGdmaXBfbm9tYnJlX3BhcnRzIiwiZGdmaXBfbm9tYnJlX3BlcnNvbm5lc19hX2NoYXJnZSIsImRnZmlwX3NpdHVhdGlvbl9mYW1pbGlhbGUiLCJkZ2ZpcF9yZXZlbnVfYnJ1dF9nbG9iYWwiLCJkZ2ZpcF9yZXZlbnVfaW1wb3NhYmxlIiwiZGdmaXBfaW1wb3RfcmV2ZW51X25ldF9hdmFudF9jb3JyZWN0aW9ucyIsImRnZmlwX21vbnRhbnRfaW1wb3QiLCJkZ2ZpcF9yZXZlbnVfZmlzY2FsX3JlZmVyZW5jZSIsImRnZmlwX2FubmVlX2ltcG90IiwiZGdmaXBfYW5uZWVfcmV2ZW51cyIsImRnZmlwX2VycmV1cl9jb3JyZWN0aWYiLCJkZ2ZpcF9zaXR1YXRpb25fcGFydGllbGxlIl0sInN1YiI6InN0YWdpbmcgZGV2ZWxvcG1lbnQiLCJpYXQiOjE2NjY4NjQwNzQsInZlcnNpb24iOiIxLjAiLCJleHAiOjE5ODI0ODMyNzR9.u2kMWzll3iCTczUOqMQbpS66VfrVzI2lLiyGEPcKAec'

# Environment variable API_ENTREPRISE_DEFAULT_SIRET  (see: .env file)
$ds_api_entreprise_default_siret = '44378317000022'

# Environment variable AR_ENCRYPTION_*  (see: .env file)
$ds_active_record_encryption_key_derivation_salt = 'test-RgUyzplf0kehB5fyZpmCd37uvgb'
$ds_active_record_encryption_primary_key  = 'test-yyMmzM9cTSD1rs3Fq3hwt3hMNg4'

# #############################################################################
# System user for Démarches Simplifiées app
accounts::user { $_system_username:
}

# #############################################################################
# TIMEZONE and Clock synchronization
# - Clock synchronization is required for OTP (super-admin login)
# - Use Europe/Paris timezone
include ntp
exec { 'timedatectl set-timezone':
  command => "/usr/bin/timedatectl set-timezone Europe/Paris",
  unless  => "/usr/bin/timedatectl | grep Europe/Paris",
}

# #############################################################################
# PREREQUISITES Postgres
# Install all global package required
$preRequistes = [
  'curl',
  'autoconf',
  'bison',
  'chromium-browser',
  'chromium-chromedriver',
  'libcurl4-openssl-dev',
  'libpq-dev',
  'libicu-dev',
]

package { $preRequistes:
  ensure  => 'installed',
  require => Class['apt::update'],
}

# Create a soft link on /shared_dev directory which is a share folder of your local project.
# Comment this part if you want to clone a repository installation.
file { $_ds_project_directory:
  ensure => link,
  target => "$local_source_code_of_ds",
}

# Desactivation of IPv6
sysctl { 'net.ipv6.conf.all.autoconf':
  ensure => present,
  value  => '1',
}

# #############################################################################
# PREREQUISITES Create and customize values of DB in .env
# Copy the file `config/env.example`, rename it to `.env` and put it at the root of the project as 'ds' user
# NOTE: once this code is transformed in a clear Puppet Module, all the file_lines should be treated with
#       content => epp()
file { "$_ds_project_directory/.env":
  ensure  => present,
  source  => "$_ds_project_directory/config/env.example",
  # content => epp('vagrant-demarches-simplifiees/env.epp'),
  owner   => $_system_username,
  require => File[$_ds_project_directory],
}

# As long as this is not a pupet module, we don't use a template but 'file_line'.
# Puppuet module should use a template to set proper values.
# Set values for Database and Github Oauth token in config file '.env'
file_line { '.env file DB_DATABASE':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "DB_DATABASE=\"$db_name\"",
  match   => '^DB_DATABASE="tps_development"',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file DB_USERNAME':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "DB_USERNAME=\"$db_user_name\"",
  match   => '^DB_USERNAME="tps_development"',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file DB_PASSWORD':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "DB_PASSWORD=\"$db_user_password\"",
  match   => '^DB_PASSWORD="tps_development"',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file ENCRYPTION_SERVICE_SALT':
  # mandatory to use API Particulier
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "ENCRYPTION_SERVICE_SALT=\"$ds_encryption_service_salt\"",
  match   => '^ENCRYPTION_SERVICE_SALT=""',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file API_ENTREPRISE_KEY':
  # mandatory to use API Entreprise
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "API_ENTREPRISE_KEY=\"$ds_api_entreprise_key\"",
  match   => '^API_ENTREPRISE_KEY=""',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file AR_ENCRYPTION_PRIMARY_KEY':
    # mandatory to ActiveRecord encryption keys
    ensure  => present,
    path    => "$_ds_project_directory/.env",
    line    => "AR_ENCRYPTION_PRIMARY_KEY=\"$ds_active_record_encryption_primary_key\"",
    match   => '^AR_ENCRYPTION_PRIMARY_KEY=""',
    require => File["$_ds_project_directory/.env"],
}
file_line { '.env file AR_ENCRYPTION_KEY_DERIVATION_SALT':
    # mandatory to ActiveRecord encryption keys
    ensure  => present,
    path    => "$_ds_project_directory/.env",
    line    => "AR_ENCRYPTION_KEY_DERIVATION_SALT=\"$ds_active_record_encryption_key_derivation_salt\"",
    match   => '^AR_ENCRYPTION_KEY_DERIVATION_SALT=""',
    require => File["$_ds_project_directory/.env"],
}
file_line { '.env file API_ENTREPRISE_DEFAULT_SIRET':
    # mandatory to use API Entreprise
    ensure  => present,
    path    => "$_ds_project_directory/.env",
    line    => "API_ENTREPRISE_DEFAULT_SIRET=\"$ds_api_entreprise_default_siret\"",
    match   => '^API_ENTREPRISE_DEFAULT_SIRET="put_your_own_siret"',
    require => File["$_ds_project_directory/.env"],
}

# Optional environment variables
file_line { '.env file, optional environment variables':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => "\n##################################\n\n# Optional environment variables",
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file APPLICATION_NAME':
    ensure  => present,
    path    => "$_ds_project_directory/.env",
    line    => 'APPLICATION_NAME="DS Vagrant DEV"',
    require => File["$_ds_project_directory/.env"],
}
file_line { '.env file APPLICATION_SHORTNAME':
    ensure  => present,
    path    => "$_ds_project_directory/.env",
    line    => 'APPLICATION_SHORTNAME="ds.dev"',
    require => File["$_ds_project_directory/.env"],
}
file_line { '.env file SUPER_ADMIN_OTP_DISABLED':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => 'SUPER_ADMIN_OTP_ENABLED="disabled" # "enabled" par défaut',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file API_PARTICULIER_URL':
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => 'API_PARTICULIER_URL="https://staging.particulier.api.gouv.fr/api"',
  require => File["$_ds_project_directory/.env"],
}
file_line { '.env file API_ENTREPRISE_URL':
  # see: config/initializers/urls.rb file
  #      https://api.gouv.fr/documentation/api-entreprise
  ensure  => present,
  path    => "$_ds_project_directory/.env",
  line    => 'API_ENTREPRISE_URL="https://staging.entreprise.api.gouv.fr"',
  require => File["$_ds_project_directory/.env"],
}
# file_line { '.env file ZONAGE_ENABLED':
#     # zonage disabled by default if `ZONAGE_ENABLED` not set
#     ensure  => present,
#     path    => "$_ds_project_directory/.env",
#     line    => 'ZONAGE_ENABLED="enabled"',
#     require => File["$_ds_project_directory/.env"],
# }

# Modification of bin/setup file.
# As we already created the DB we don't need to recreate it.
file_line { 'db:prepare':
  ensure  => present,
  path    => "$_ds_project_directory/bin/setup",
  line    => '  system! "bin/rails db:schema:load db:seed"',
  match   => '  system! "bin/rails db:prepare"',
  require => File[$_ds_project_directory],
}
# Disable [ bin/rails webdrivers:chromedriver:update ]
file_line { 'remove webdrivers:chromedriver:update':
    ensure  => present,
    path    => "$_ds_project_directory/bin/setup",
    line    => "#  system! 'RAILS_ENV=test bin/rails webdrivers:chromedriver:update'",
    match   => "  system! 'RAILS_ENV=test bin/rails webdrivers:chromedriver:update'",
    require => File[$_ds_project_directory],
}

# #############################################################################
# PREREQUISITES ruby
# Install rbenv
class { 'rbenv':
  install_dir => "$_ds_home/rbenv",
  owner       => $_system_username,
}
rbenv::plugin { 'rbenv/ruby-build': }
-> rbenv::build { '3.2.2': global => true }

# Ensure permissions on /usr/bin/env
-> file { '/usr/bin/env':
  ensure => file,
  owner  => "$_system_username",
}

# #############################################################################
# PREREQUISITES PostgreSQL
include postgresql::server

# postgresql::server::db { $db_name:
#     user     => $db_user,
#     password => postgresql::postgresql_password($db_user, $db_password),
# }

# Grant membership to a role
postgresql::server::role { $_ds_db_role:
  password_hash => postgresql_password($db_user_name, $db_user_password),
}
# Create a role, database and assign the correct permissions
-> postgresql::server::db { $db_name:
  user     => $db_user_name,
  password => postgresql_password($db_user_name, $db_user_password),
  owner    => $_ds_db_role,
}

# Role Test creation
postgresql::server::role { $_ds_db_test_role:
  password_hash => postgresql_password($db_test_user_name, $db_test_user_password),
}
# DB Test + Test Postgres user creation + assignation permissions
-> postgresql::server::db { $db_test_name:
  user     => $db_test_user_name,
  password => postgresql_password($db_test_user_name, $db_test_user_password),
  owner    => $_ds_db_test_role,
}

# Activate an extension 'unaccent' on postgresql databases (both ds and tps_test)
[$db_test_name, $db_name].each | $database | {
  postgresql::server::extension { "unaccent-${database}":
    database  => $database,
    extension => 'unaccent',
    ensure    => present,
  }
  postgresql::server::extension { "pgcrypto-${database}":
    database  => $database,
    extension => 'pgcrypto',
    ensure    => present,
  }
}

# PGPASS file
$_pgpass_content = "
# hostname:port:database:username:password
${db_host}:${db_port}:${db_name}:${db_user_name}:${db_user_password}
${db_host}:${db_port}:${db_test_name}:${db_test_user_name}:${db_test_user_password}
"

file { 'PGPASS file':
  ensure  => file,
  path    => "${_ds_home}/.pgpass",
  mode    => '0600',
  owner   => $_system_username,
  content => $_pgpass_content,
}

# #############################################################################
# PREREQUISITES nodejs
# The desired version must be specified.
class { 'nodejs':
  repo_url_suffix => '18.x',
}

# #############################################################################
# PREREQUISITES Yarn
# Install Yarn
# /!\ By default, last version is installed
class { 'yarn': }

